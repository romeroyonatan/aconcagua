from abc import ABCMeta, abstractmethod, ABC
from fractions import Fraction
from typing import Union
from unittest import TestCase


class TimeMetaclass(ABCMeta):
    def __rmul__(self, value):
        return self(value)


class TimeUnit(metaclass=ABCMeta):
    @abstractmethod
    def as_milliseconds(self):
        pass

    @abstractmethod
    def as_seconds(self):
        pass

    @abstractmethod
    def as_minutes(self):
        pass

    @abstractmethod
    def as_hours(self):
        pass

    @abstractmethod
    def as_days(self):
        pass


class TimeMeasure(TimeUnit, metaclass=TimeMetaclass):
    TIME_UNIT_IS_IMMUTABLE = "Time unit is immutable"
    PLURAL_TEMPLATE: str
    SINGULAR_TEMPLATE: str
    _value: Union[int, float]

    def __init__(self, value):
        super().__setattr__("_value", value)

    def __str__(self):
        value = self._try_to_convert_value_to_int()
        if value == 1:
            return self.SINGULAR_TEMPLATE.format(value)
        return self.PLURAL_TEMPLATE.format(value)

    def _try_to_convert_value_to_int(self):
        if isinstance(self._value, float) and self._value.is_integer():
            return int(self._value)
        return self._value

    def __setattr__(self, key, value):
        raise AttributeError(TimeMeasure.TIME_UNIT_IS_IMMUTABLE)

    def __repr__(self):
        return "{} * {}".format(self._value, self.__class__.__name__)

    def __add__(self, other):
        if not isinstance(other, TimeUnit):
            return NotImplemented
        if isinstance(other, self.__class__):
            return self.__class__(self._value + other._value)
        return TimeUnitAddition(self, other)

    def __sub__(self, other):
        if not isinstance(other, TimeUnit):
            return NotImplemented
        if isinstance(other, self.__class__):
            return self.__class__(self._value - other._value)
        return TimeUnitSubtraction(self, other)

    def __int__(self):
        return int(self._value)

    def __float__(self):
        return float(self._value)

    def _is_equals(self, other, time_converter):
        if isinstance(other, self.__class__):
            return self._value == other._value
        if isinstance(other, TimeUnit):
            return self == time_converter(other)
        return False


class Millisecond(TimeMeasure):
    SINGULAR_TEMPLATE = "{} millisecond"
    PLURAL_TEMPLATE = "{} milliseconds"

    def __eq__(self, other):
        return self._is_equals(other, lambda a_time: a_time.as_milliseconds())

    def as_milliseconds(self):
        return self

    def as_seconds(self):
        return Second(Fraction(self._value, 1000))

    def as_minutes(self):
        return self.as_seconds().as_minutes()

    def as_hours(self):
        return self.as_seconds().as_hours()

    def as_days(self):
        return self.as_hours().as_days()


class Second(TimeMeasure):
    SINGULAR_TEMPLATE = "{} second"
    PLURAL_TEMPLATE = "{} seconds"

    def __eq__(self, other):
        return self._is_equals(other, lambda a_time: a_time.as_seconds())

    def as_milliseconds(self):
        return Millisecond(self._value * 1000)

    def as_seconds(self):
        return self

    def as_minutes(self):
        return Minute(Fraction(self._value, 60))

    def as_hours(self):
        return Hour(Fraction(self._value, 3600))

    def as_days(self):
        return self.as_hours().as_days()


class Minute(TimeMeasure):
    SINGULAR_TEMPLATE = "{} minute"
    PLURAL_TEMPLATE = "{} minutes"

    def __eq__(self, other):
        return self._is_equals(other, lambda a_time: a_time.as_minutes())

    def as_milliseconds(self):
        return self.as_seconds().as_milliseconds()

    def as_seconds(self):
        return Second(self._value * 60)

    def as_minutes(self):
        return self

    def as_hours(self):
        return Hour(Fraction(self._value, 60))

    def as_days(self):
        return self.as_hours().as_days()


class Hour(TimeMeasure):
    SINGULAR_TEMPLATE = "{} hour"
    PLURAL_TEMPLATE = "{} hours"

    def __eq__(self, other):
        return self._is_equals(other, lambda a_time: a_time.as_hours())

    def as_milliseconds(self):
        return self.as_seconds().as_milliseconds()

    def as_seconds(self):
        return Second(self._value * 3600)

    def as_minutes(self):
        return Minute(self._value * 60)

    def as_hours(self):
        return self

    def as_days(self):
        return Day(Fraction(self._value, 24))


class Day(TimeMeasure):
    SINGULAR_TEMPLATE = "{} day"
    PLURAL_TEMPLATE = "{} days"

    def __eq__(self, other):
        return self._is_equals(other, lambda a_time: a_time.as_days())

    def as_milliseconds(self):
        return self.as_hours().as_milliseconds()

    def as_seconds(self):
        return self.as_hours().as_seconds()

    def as_minutes(self):
        return self.as_hours().as_minutes()

    def as_hours(self):
        return Hour(self._value * 24)

    def as_days(self):
        return self


class TimeUnitAddition(TimeUnit):
    def __init__(self, first_time: TimeUnit, second_time: TimeUnit):
        self.first_time = first_time
        self.second_time = second_time

    def __str__(self):
        return "{} + {}".format(self.first_time, self.second_time)

    def __repr__(self):
        return "{} + {}".format(repr(self.first_time), repr(self.second_time))

    def __add__(self, other):
        if not isinstance(other, TimeUnit):
            return NotImplemented
        return TimeUnitAddition(self, other)

    def __eq__(self, other):
        if not isinstance(other, TimeUnit):
            return False
        return self.as_seconds() == other.as_seconds()

    def __sub__(self, other):
        if not isinstance(other, TimeUnit):
            return NotImplemented
        return TimeUnitSubtraction(self, other)

    def as_milliseconds(self):
        return self.as_seconds().as_milliseconds()

    def as_seconds(self):
        return self.first_time.as_seconds() + self.second_time.as_seconds()

    def as_minutes(self):
        return self.first_time.as_minutes() + self.second_time.as_minutes()

    def as_hours(self):
        return self.first_time.as_hours() + self.second_time.as_hours()

    def as_days(self):
        return self.first_time.as_days() + self.second_time.as_days()


class TimeUnitSubtraction(TimeUnit):
    def __init__(self, first_time: TimeUnit, second_time: TimeUnit):
        self.first_time = first_time
        self.second_time = second_time

    def __eq__(self, other):
        if not isinstance(other, TimeUnit):
            return False
        return self.as_seconds() == other.as_seconds()

    def __str__(self):
        return "{} - {}".format(self.first_time, self.second_time)

    def __repr__(self):
        return "{} - {}".format(repr(self.first_time), repr(self.second_time))

    def __add__(self, other):
        if not isinstance(other, TimeUnit):
            return NotImplemented
        return TimeUnitAddition(self, other)

    def __sub__(self, other):
        if not isinstance(other, TimeUnit):
            return NotImplemented
        return TimeUnitSubtraction(self, other)

    def as_milliseconds(self):
        return self.first_time.as_milliseconds() - self.second_time.as_milliseconds()

    def as_seconds(self):
        return self.first_time.as_seconds() - self.second_time.as_seconds()

    def as_minutes(self):
        return self.first_time.as_minutes() - self.second_time.as_minutes()

    def as_hours(self):
        return self.first_time.as_hours() - self.second_time.as_hours()

    def as_days(self):
        return self.first_time.as_days() - self.second_time.as_days()


class TimeTest(TestCase):
    def test1(self):
        self.assertEqual(1 * Second, Second(1))

    def test2(self):
        self.assertEqual(str(1 * Second), "1 second")

    def test3(self):
        self.assertEqual(str(2 * Second), "2 seconds")

    def test4(self):
        three_seconds = 2 * Second + 1 * Second
        self.assertEqual(three_seconds, 3 * Second)

    def test5(self):
        self.assertEqual(1 * Minute, Minute(1))

    def test6(self):
        self.assertEqual(str(1 * Minute), "1 minute")

    def test7(self):
        self.assertEqual(str(2 * Minute), "2 minutes")

    def test8(self):
        self.assertEqual(1 * Minute + 2 * Minute, 3 * Minute)

    def test9(self):
        self.assertEqual(eval(repr(60 * Second)), 60 * Second)

    def test10(self):
        self.assertEqual(str(1 * Minute + 2 * Second), "1 minute + 2 seconds")

    def test11(self):
        one_second = 1 * Second
        self.assertEqual(one_second.as_seconds(), 1 * Second)

    def test12(self):
        one_minute = 1 * Minute
        self.assertEqual(one_minute.as_seconds(), 60 * Second)

    def test13(self):
        a_time_operation = 1 * Minute + 2 * Second
        self.assertEqual(a_time_operation.as_seconds(), 62 * Second)

    def test14(self):
        a_time_operation = 1 * Minute + 60 * Second
        self.assertEqual(a_time_operation.as_minutes(), 2 * Minute)

    def test15(self):
        self.assertEqual(1 * Minute, 60 * Second)

    def test16(self):
        self.assertEqual(60 * Second, 1 * Minute)

    def test17(self):
        actual = 2 * Second + 1 * Minute + 59 * Second + 1 * Hour
        expected = 62 * Minute + 1 * Second
        self.assertEqual(actual, expected)

    def test18(self):
        a_second = 1 * Second

        with self.assertRaises(AttributeError) as error:
            a_second._value = 2

        self.assertEqual(str(error.exception), TimeMeasure.TIME_UNIT_IS_IMMUTABLE)
        self.assertEqual(a_second, 1 * Second)

    def test19(self):
        self.assertEqual(60 * Minute, 1 * Hour)

    def test20(self):
        self.assertEqual(3600 * Second, 1 * Hour)

    def test21(self):
        self.assertEqual(1 * Hour, 60 * Minute)

    def test22(self):
        self.assertEqual(1 * Hour, 3600 * Second)

    def test23(self):
        one_hour = 1 * Hour

        actual = int(one_hour.as_seconds())
        expected = 3600
        self.assertEqual(actual, expected)

    def test24(self):
        half_hour = 30 * Minute

        actual = float(half_hour.as_hours())
        expected = 0.5
        self.assertEqual(actual, expected)

    def test25(self):
        self.assertEqual(str(0.5 * Hour), "0.5 hours")

    def test26(self):
        self.assertEqual(str(1.0 * Hour), "1 hour")

    def test27(self):
        self.assertEqual(str((3600 * Second).as_hours()), "1 hour")

    def test28(self):
        self.assertEqual(1 * Second, 1000 * Millisecond)

    def test29(self):
        self.assertEqual(1 * Millisecond, 1 / 1000 * Second)

    def test30(self):
        self.assertEqual(1 * Day, 24 * Hour)

    def test31(self):
        one_millisecond = 1 * Millisecond
        self.assertEqual(one_millisecond.as_days().as_milliseconds(), 1 * Millisecond)

    def test32(self):
        self.assertEqual(1 * Day - 1 * Hour, 23 * Hour)

    def test33(self):
        self.assertEqual(1 * Day - 1 * Minute, 23 * Hour + 59 * Minute)

    def test34(self):
        self.assertEqual(1 * Day + 2 * Second - 1 * Second - 1 * Second, 24 * Hour)
